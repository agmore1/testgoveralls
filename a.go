package main
import (
  "fmt"
)

func Test(a int) bool {
    return a == 123
}

func main() {
    fmt.Println(Test(123))
}
